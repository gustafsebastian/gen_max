<?php
$servername = "localhost";
$serverUsername = "root"; //u7693207
$serverPassword = ""; //
$dbname="u7693207_youthermon";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$dbname", $serverUsername, $serverPassword);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    //echo "Connected"."<br>"; 
}
catch(PDOException $e){
    echo "Connection failed: " . $e->getMessage();
}
?>
