<?php $_SESSION['week_id']= $current_wv['week_id']; ?>

<div ng-controller="listContactCtrl">
   	<div class="modal fade" id="post_gen_max" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   		<div class="modal-dialog" role="document">
   			<div class="modal-content">
		        <form id="post_gen_max" name="post_gen_max" action="act/post_gen_max-action.php" method="post" onSubmit="return validation()">
		    		<div class="modal-header">
		    			<h4 class="modal-title" id="myModalLabel">Add New Post</h4>
		    			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		    				<span aria-hidden="true">×</span>
		    			</button>
		    		</div>
		    		<div class="modal-body">
		    		<!-- Weekly Verse -->
						<div id="weekly_verse" class="form-group">
						    <label>Weekly Verse : <?=$current_wv['verse'] ?></label>
						</div>
					<!-- Verse Address -->
						<div id="verse_address" class="form-group">
							<label><b> Verse Address </b></label>
						    <input type="text" class="form-control" id="inputVerseAddress" name="inputVerseAddress" placeholder="Please input verse address"/>
						</div>
					<!-- Verse Content -->
						<div id="verse_content" class="form-group">
							<label><b> Verse Content </b></label>
							<textarea class="form-control rounded-0" id="inputVerseContent" name="inputVerseContent" rows="3" placeholder="Please input verse Content"></textarea>
						</div>
					<!-- Reflection -->
		    			<div id="reflection" name="reflection" class="form-group">
							<label><b> Reflection </b></label>
		    				<textarea class="form-control rounded-0" id="inputReflection" name="inputReflection" rows="5" placeholder="Please input Reflection"></textarea>
		    			</div>
			        </div>
			        <div class="modal-footer">
			        <!-- Button -->
			            <button type="submit" class="btn btn-success">Submit</button>
			            <!-- <input type="reset" value="Reset" onclick="" class="btn btn-warning btn-small"/> -->
			        </div>
		        </form>
    		</div>
    	</div>
    </div>

<script type="text/javascript">
	function validation() {
		var x = document.forms["post_gen_max"]["weekly_verse"].value;
		if (x == null || x == "") {
			alert("Weekly Verse can't be empty");
			return false;
		}

		var x = document.forms["post_gen_max"]["verse_address"].value;
		if (x == null || x == "") {
			alert("Verse_address can't be empty");
			return false;
		}

		var x = document.forms["post_gen_max"]["verse_content"].value;
		if (x == null || x == "") {
			alert("Verse_content can't be empty");
			return false;
		}

		var x = document.forms["post_gen_max"]["reflection"].value;
		if (x == null || x == "") {
			alert("Reflection can't be empty");
			return false;
		}
	}
</script>    
</div>