<div ng-controller="listContactCtrl">
   	<div class="modal fade" id="modal_register_account" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
   		<div class="modal-dialog" role="document">
   			<div class="modal-content">
		        <form id="register_account" name="register_account" action="act/register_account-action.php" method="post" onSubmit="return validation()">
		    		<div class="modal-header modal-header-regis">
		    			<h4 class="modal-title" id="myModalLabel">Register Form</h4>
		    			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		    				<span aria-hidden="true">×</span>
		    			</button>
		    		</div>
		    		<div class="modal-body modal-body-regis">		    	
					<!-- Input Username -->
						<div id="regis-username" class="form-group">
                            <label><b> Username </b></label>
						    <input type="text" class="form-control" id="inputRegisUsername" name="inputRegisUsername" placeholder="Your Username"/>
                        </div>
                    <!-- Input Name -->
                    <div id="regis-name" class="form-group">
                        <label><b> Name </b></label>
                        <input type="text" class="form-control" id="inputRegisName" name="inputRegisName" placeholder="Your Name"/>
                    </div>
                    <!-- Input Gender -->
                    <div id="regis-gender" class="form-group">
                        <label><b> Gender </b></label> <br>
                        <input type="radio" class="" id="inputRegisGender" name="gender" value="male"> Male
                        <input type="radio" class="" id="inputRegisGender" name="gender" value="female"> Female <br>
                    </div>
                    <!-- Input Birthdate -->
                    <div id="regis-birth" class="form-group">
                        <label><b> Birth Date </b></label>
                        <input type="date" class="form-control" id="inputRegisBirthdate" name="inputRegisBirthdate" placeholder="Your Birthdate"/>
                    </div>
                    <!-- Input Address -->
                    <div id="regis-address" class="form-group">
                        <label><b> Address </b></label>
                        <input type="text" class="form-control" id="inputRegisAddress" name="inputRegisAddress" placeholder="Your Address"/>
                    </div>
                    <!-- Input Email -->
                    <div id="regis-email" class="form-group">
                        <label><b> Email </b></label>
                        <input type="text" class="form-control" id="inputRegisEmail" name="inputRegisEmail" placeholder="Your Email"/>
                    </div>
                    <!-- Input WhatsApp / Phone -->
                    <div id="regis-phone" class="form-group">
                        <label><b> WhatsApp / Phone </b></label>
                        <input type="text" class="form-control" id="inputRegisPhone" name="inputRegisPhone" placeholder="Your Phone"/>
                    </div>
                    <!-- Input Password -->
                    <div id="regis-password" class="form-group">
                        <label><b> Password </b></label>
                        <input type="password" class="form-control" id="inputRegisPassword" name="inputRegisPassword" placeholder="Your Password"/>
                    </div>
                    <!-- Input Confirm Password -->
                    <div id="regis-confirmpassword" class="form-group">
                        <label><b> Confirm Password </b></label>
                        <input type="password" class="form-control" id="inputRegisCPassword" name="inputRegisCPassword" placeholder="Re-type Password"/>
                    </div>
			        <div class="modal-footer">
			        <!-- Button -->
			            <button type="submit" class="btn-register"> Register</button>
			            <!-- <input type="reset" value="Reset" onclick="" class="btn btn-warning btn-small"/> -->
			        </div>
		        </form>
    		</div>
    	</div>
    </div>

<script type="text/javascript">
	function validation() {
		var x = document.forms["register_account"]["regis-username"].value;
		if (x == null || x == "") {
			alert("Username can't be empty");
			return false;
		}

		var x = document.forms["register_account"]["regis-name"].value;
		if (x == null || x == "") {
			alert("Name can't be empty");
			return false;
		}

		var x = document.forms["register_account"]["regis-gender"].value;
		if (x == null || x == "") {
			alert("Gender can't be empty");
			return false;
		}

		var x = document.forms["register_account"]["regis-birth"].value;
		if (x == null || x == "") {
			alert("Birth Date can't be empty");
			return false;
		}

        var x = document.forms["register_account"]["regis-address"].value;
		if (x == null || x == "") {
			alert("Address can't be empty");
			return false;
		}

        var x = document.forms["register_account"]["regis-email"].value;
		if (x == null || x == "") {
			alert("Email can't be empty");
			return false;
		}

        var x = document.forms["register_account"]["regis-phone"].value;
		if (x == null || x == "") {
			alert("Phone can't be empty");
			return false;
		}

        var x = document.forms["register_account"]["regis-password"].value;
		if (x == null || x == "") {
			alert("Password can't be empty");
			return false;
		}

        var x = document.forms["register_account"]["regis-confirmpassword"].value;
		if (x == null || x == "") {
			alert("Confirm Password can't be empty");
			return false;
		}
	}
</script>    
</div>