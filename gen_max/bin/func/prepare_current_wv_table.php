<?php
    $w_sql = "SELECT   wv.id AS week_id,
                        wv.verse AS verse,
                        wp.start_date AS start,
                        wp.end_date AS last
            FROM weekly_verse AS wv INNER JOIN week_period AS wp ON wv.id = wp.id WHERE wp.start_date <= '$today' and '$today' <= wp.end_date";
    $w = $conn->query($w_sql);
    $w->execute();

    if($w->rowCount()<=1){
        $w->setFetchMode(PDO::FETCH_ASSOC);
        $current_wv= $w->fetch();
    }
    else
        echo "more than 1 Result found";

    /*=============================================
            CREATE TABLE WV
            AND COLUMN:
    1. week_id
    2. verse
    3. start
    4. last
    =============================================*/
?>