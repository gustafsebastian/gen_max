<?php
	session_start();
	$login_username=$_SESSION['username']; // Declare

	$us = $conn->prepare('SELECT * FROM users WHERE username = :username');
	$us->execute(array(':username' => $login_username));
	$user=$us->fetch(PDO::FETCH_ASSOC);

	$login_userid=$user['id']; // Declare

	if(!isset($login_username)){
		header("Location: login");
	}
	else if(!password_verify($_SESSION['password'], $user['password'])){
			header("location: bin/func/logout");
		}
?>