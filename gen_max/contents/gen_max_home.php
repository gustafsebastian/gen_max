<?php
    $minimum=$current_wv['week_id']-1;

    // Get USER DATA
    $sql = "SELECT  gm.id AS id,
                    gm.user_id AS user_id,
                    gm.weekly_verse_id AS weekly_verse_id,
                    gm.verse_address AS verse_address,
                    gm.verse_content AS verse_content,
                    gm.reflection AS reflection,
                    gm.created_on AS created_on,
                    gm.last_updated AS last_updated,
                    u.username AS username,
                    u.picture_name AS picture_name,
                    u.name AS name,
                    u.cell_id AS community_cell,
                    u.address AS address,
                    u.birth_date AS birthdate,
                    u.phone AS phone,
                    u.usertype AS type,
                    u.last_login AS last_login

            FROM gen_max AS gm INNER JOIN users AS u ON gm.user_id = u.id WHERE gm.weekly_verse_id >= $minimum ORDER BY gm.last_updated DESC";
    $gm = $conn->query($sql);
    $gm->setFetchMode(PDO::FETCH_ASSOC); ?>

    <ul class="list-group list-group-flush">
<?php
    while($gen_max=$gm->fetch()){ ;?>
        <li class="list-group-item contents">
            <div class="row" style="border-style: dotted;">
                <div class="row">
                    <div class="col-xs-2 col-md-2 col-4"> 
                        <img class="pp-img" src="../android/profile_picture/<?=$gen_max['picture_name'];?>"> 
                    </div>
                    <div class="col-xs-10 col-md-10 col-8">
                        <div class="col-md-12 font-weight-bold" id="u_name"><?=$gen_max['name'];?></div>
                        <div class="col-md-12" id="u_verse_addr"><?=$gen_max['verse_address'];?></div>
                        <div class="col-md-12 info" id="u_created">Created: <?=$gen_max['created_on'];?></div>
                        <div class="col-md-12 info" id="u_updated">Updated: <?=$gen_max['last_updated'];?></div>
                        <!-- <div class="col-md-12 info" id="">Gen Max Status:  </div> -->
                    </div>
                    <div class="stats">
                        <br>
                        <div class="col-md-12 info" id="" style="font-weight:bold;">My Info:</div>
                        <div class="col-md-12 info" id="">Status:
                        <?php
                            $status = $gen_max['type'];
                            if($status == 0) {
                                echo "SuperAdmin";
                            }
                            elseif($status == 1) {
                                echo "Elder";
                            }
                            elseif ($status == 2) {
                                echo "Admin";
                            }
                            elseif ($status == 3) {
                                echo "Community Cell Leader";
                            }
                            elseif ($status == 4) {
                                echo "Member";
                            }
                            elseif ($status == 5) {
                                echo "Guest";
                            }
                        ?>                    
                        </div>
                        <!-- <div class="col-md-12 info" id="">Community Cell: <?=$gen_max['community_cell'];?></div> -->
                        <div class="col-md-12 info" id="">Phone: <?=$gen_max['phone'];?></div>
                        <div class="col-md-12 info" id="">Address: <?=$gen_max['address'];?></div>
                        <div class="col-md-12 info" id="">Birthdate: <?=$gen_max['birthdate'];?></div> 
                        <!-- <div class="col-md-12 info" id="">Last Login: <?=$gen_max['last_login'];?></div> -->
                    </div>
                </div>
                <div class="content-gm">
                    <br>
                    <p class="col-md-12 info-2" style="font-weight:bold;"> Verse Content : </p>
                    <div class="col-md-12 info-2" style="margin-top:20px;"><?=$gen_max['verse_content'];?></div> <br>
                    <p class="col-md-12 info-2" style="font-weight:bold;"> Reflection : </p>
                    <div class="col-md-12 info-2"><?=$gen_max['reflection'];?>
                </div>
            </div>
        </li>
<?php   
    }?>
    </ul>