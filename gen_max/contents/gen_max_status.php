<?php
    // Get USER DATA
    $sql = "SELECT  gm.id AS id,
                    gm.user_id AS user_id,
                    gm.weekly_verse_id AS weekly_verse_id,
                    MAX(gm.created_on) AS created_on,
                    gm.last_updated AS last_updated,
                    u.username AS username,
                    u.cell_id AS cell_id,
                    u.address AS address,
                    u.phone AS phone,
                    u.picture_name AS picture_name,
                    u.name AS name,
                    cc.name AS cell_name,
                    cc.leader_id AS leader_id
            FROM gen_max AS gm  INNER JOIN users AS u ON gm.user_id = u.id 
                                INNER JOIN community_cell AS cc ON u.cell_id = cc.id
                                    WHERE gm.created_on >= '$today' GROUP BY gm.user_id";
    $cl = $conn->query($sql);
    $cl->setFetchMode(PDO::FETCH_ASSOC);
    
    while($clear=$cl->fetch()){ ;?>
        <li class="list-group-item">
            <div class="row">
                <div class="col-md-2 col-4"> <img class="pp-img" src="../android/profile_picture/<?=$clear['picture_name'];?>"> </div>
                <div class="col-md-10 col-8">
                    <div class="col-md-12 font-weight-bold"><?=$clear['name'];?></div>
                    <div class="col-md-12 font-weight-bold"><?=$clear['cell_name'];?></div>
                    <div class="col-md-12">Phone  : <?=$clear['phone'];?></div>
                    <div class="col-md-12">Address: <?=$clear['address'];?></div>
                    <div class="col-md-12">Created: <?=$clear['created_on'];?></div>
                    <div class="col-md-12">Updated: <?=$clear['last_updated'];?></div>
                </div>
            </div>
        </li>
<?php   
    }?>