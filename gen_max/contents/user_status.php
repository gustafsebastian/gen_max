<?php
    $minimum=$current_wv['week_id']-1;
    
    // Get USER DATA
    $sql = "SELECT  gm.id AS id,
                    gm.user_id AS user_id,
                    gm.weekly_verse_id AS weekly_verse_id,
                    MAX(gm.created_on) AS created_on,
                    MAX(gm.last_updated) AS last_updated,
                    u.username AS username,
                    u.usertype AS type,
                    u.cell_id AS cell_id,
                    u.address AS address,
                    u.phone AS phone,
                    u.picture_name AS picture_name,
                    u.name AS name,
                    cc.name AS cell_name,
                    cc.leader_id AS leader_id
            FROM gen_max AS gm  INNER JOIN users AS u ON gm.user_id = u.id 
                                INNER JOIN community_cell AS cc ON u.cell_id = cc.id
                                    WHERE gm.weekly_verse_id >= $minimum GROUP BY gm.user_id ORDER BY u.usertype ASC, u.name ASC";
    $cl = $conn->query($sql);
    $cl->setFetchMode(PDO::FETCH_ASSOC); ?>
    
<div class="container-fluid">
<?php    
    while($clear=$cl->fetch()){
        if($clear['created_on']<$today){ ?>
            <li class="list-group-item">
                <div class="row" style="border-style: dotted;">
                    <div class="col-md-2 col-4"> 
                        <img class="pp-img2" src="../android/profile_picture/<?=$clear['picture_name'];?>"> 
                    </div>
                    <div class="col-md-10 col-8">
                        <div class="col-md-12 font-weight-bold" id="u_name2" style="margin-top: 10px;"><?=$clear['name'];?></div>
                        <div class="col-md-12 font-weight-bold"><?=$clear['cell_name'];?></div>
                        <!-- <div class="col-md-12" style="color: blue;">Status:</div> -->
                    </div>
                    <div class="col-12" id="" style="margin-top: 15px;">Status :
                        <?php
                            $status = $clear['type'];
                            if($status == 0) {
                                echo "SuperAdmin";
                            }
                            elseif($status == 1) {
                                echo "Pembina";
                            }
                            elseif ($status == 2) {
                                echo "Pengurus";
                            }
                            elseif ($status == 3) {
                                echo "PKS";
                            }
                            elseif ($status == 4) {
                                echo "Anggota";
                            }
                            elseif ($status == 5) {
                                echo "Guest";
                            }
                        ?>                    
                    </div>
                    <div class="col-12">Phone  : <?=$clear['phone'];?></div>
                    <div class="col-12">Address : <br> <?=$clear['address'];?></div>
                    <!-- <div class="col-12">Created: <?=$clear['created_on'];?></div> -->
                    <div class="col-12">Last Post : <br> <?=$clear['last_updated'];?></div>
                </div>
            </li>
<?php   }   
    }?>
</div>