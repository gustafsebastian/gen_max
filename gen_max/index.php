<?php
    include '../tools/inc-pack.php';
    require 'bin/core_var.php';
	require $db_path;
    require 'bin/login_status.php';
    include 'bin/func/time.php';
    include 'bin/func/prepare_current_wv_table.php';
        // $login_userid = $_SESSION['user_id'];
        // $login_username=$_SESSION['username']
    include 'act/post_gen_max.php';
    include 'act/edit_gen_max.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <link rel="stylesheet" type="text/css" href="../tools/fontawesome-free-5.10.1-web/css/all.min.css">
    <link rel="stylesheet" type="text/css" href="../tools/fontawesome-free-5.10.1-web/css/index.css">
</head>
<body><div class="container-fluid">
	<div class="col-md-12" id="navbar">
<!-- 	==============================================
							NAV BAR
		============================================== -->
        <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <!-- Brand -->
            <a class="navbar-brand" href="index"><i class="fa fa-home" aria-hidden="true"></i> Home</a>

        <!-- Toggler/collapsibe Button -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
            </button>

        <!-- Navbar links -->
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                <ul class="navbar-nav">

                <!-- <li class="nav-item"><a class="nav-link" href=""> <i class="fas fa-book-open" id="weekly-icon"></i><b class="weekly"> Weekly Verse</b></a> </li> -->

<?php           include 'bin/gen_max_status.php'; // Check Gen max Status (Sudah gen max atau belum)
                // =================== Jika Belum Gen Max ===================
                if($gen_max_status==0){ 
?>                  
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="modal" data-target="#post_gen_max">
                            <i class="fas fa-plus"></i> <b class="add-post"> Add</b>
                        </a>
                    </li>
<?php           }
                // =================== Jika Sudah Gen Max ===================
                else if($gen_max_status==1){
?>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="modal" data-target="#edit_gen_max">
                            <i class="fas fa-edit"></i><b class="edit-post"> Edit</b>
                        </a>
                    </li>
<?php           } // =================== Jika Error ===================
                else{
                    echo "Error gen max ".$gen_max_status;
                } // =================== end if ===================
?>
                    <li class="nav-item"><a class="nav-link" href="index?c=1"> <i class="fas fa-list-ul" id="status-icon"></i><b class="status"> Status Gen Max</b></a> </li>
                    <li class="nav-item"><a class="nav-link" href="bin/func/logout"> <i class="fas fa-power-off" id="logout-icon"></i><b class="logout"> Logout</b></a> </li> 
                </ul>
            </div>
        </nav>

<!-- 	==============================================
						END OF NAV BAR
		============================================== -->
	</div>
	
<!--======================================================== Contents ========================================================-->
  
<?php   if(!isset($_GET['c']))
            include 'contents/gen_max_home.php';
        else if($_GET['c']==1)
            include 'contents/user_status.php';
        
?>
</div>
</body>
</html>