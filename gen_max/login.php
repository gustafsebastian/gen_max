<?php
    require 'bin/core_var.php';
	require $db_path;
	
	$login_user 	= isset($_POST['inputUsername']) ? $_POST['inputUsername'] : '';
	$login_password = isset($_POST['inputPassword']) ? $_POST['inputPassword'] : '';
// ============================================ Check Login Sesion ============================================
	session_start();
	if(isset($_SESSION['username'])){ // Check if already login 
		header("location: index");
	}
	else if($_SERVER["REQUEST_METHOD"] == "POST"){ // Check if login
		$sql="SELECT * FROM users WHERE username = :login_user";
		$stmt=$conn->prepare($sql);
		$stmt->execute([':login_user'=>$login_user]);
		$user = $stmt->fetch(PDO::FETCH_ASSOC);
		 
		if(password_verify($login_password, $user['password'])){
			$_SESSION['user_id'] = $user['id'];
			$_SESSION['username'] = $login_user;
			$_SESSION['password'] = $login_password;
			header("location: index");
		}
		else  ?> 
			<script type="text/javascript"> alert("Your Username or Password is invalid");</script>
<?php
	}
// ========================================== End of Check Login Sesion ==========================================

	include "../tools/inc-pack.php"; // include starter pack
	include "bin/core_var.php"; // include core variable
	// include 'act/register_account.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Login Page</title>
	<style type="text/css">.banner{background-color: #29292A; color:#FFF;}</style>
	<link rel="stylesheet" type="text/css" href="../tools/fontawesome-free-5.10.1-web/css/index.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

	<!-- <div class="jumbotron banner"><h1 class="text-center">Form Login <?=$appName;?></h1></div> -->
	<div class="jumbotron banner"><img src="../android/icons/logo.png" style="width: 25%; display: block; margin-left: auto; margin-right: auto;"></div>

	<form id="login_admin" action="" method="post">
		<div class="form-group col-md-6 offset-md-3">
			<div class="form-group">	
				<label class="col-md-4 control-label">Username</label>
				<div class="col-md-12">
					<input id="username-field" type="text" class="form-control" name="inputUsername" placeholder="Please input Username"/><br />
				</div>
			</div>
			<div class="form-group" style="margin-top: -20px;">
			<label class="col-md-4 control-label">Password</label>
			<div class="col-md-12">
				<input id="password-field" type="password" class="form-control" name="inputPassword" placeholder="Please input Password" />
				<span toggle="#password-field" class="fa fa-fw fa-eye-slash field-icon toggle-password" style="margin-top: -27px; float: right; padding-right: 30px;"></span><br/>
				</div>
			</div>
			 <div class="col-md-12" style="margin-top: -20px;">
				<label><input type="checkbox" id="remember"> Remember me</label>
			</div>
			<!-- <div class="col-md-12" style="margin-top: 10px;">
				<label>Don't have account ?</label>
				<a class="" style="color: red;" data-toggle="modal" data-target="#modal_register_account">Register Here</a>
			</div> -->
			<div class="col-md-12" style="margin-top: 15px;text-align: right;">
				<input type="submit" class="btn-login" value="Login"/>
			</div>
		</div>
	</form>
</body>
</html>

<script type="text/javascript">
	
	$(".toggle-password").click(function() {
		$(this).toggleClass("fa-eye-slash fa-eye");
		var input = $($(this).attr("toggle"));
		if (input.attr("type") == "password") {
			input.attr("type", "text");
		} else {
			input.attr("type", "password");
		}
	});

</script>    